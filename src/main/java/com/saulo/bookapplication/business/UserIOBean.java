/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saulo.bookapplication.business;

import com.saulo.bookapplication.bean.SessionBean;
import com.saulo.bookapplication.controller.UserBeanJpaController;
import com.saulo.bookapplication.bean.UserBean;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sf
 */
@RequestScoped
@Named(value = "userIOBean")

public class UserIOBean implements Serializable {
    
    @Inject
    UserBean userEntity;

  
    //validate login
    public String validateUsernamePassword() {
        boolean valid = UserBeanJpaController.validate(userEntity.getUname(), userEntity.getPassword());
        if (valid) {
            HttpSession session = SessionBean.getSession();
            session.setAttribute("username",userEntity.getUname());
            return "admin";
        } else {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Username and Passowrd",
                            "Please enter correct username and Password"));
            return "main";
        }
    }
 
    //logout event, invalidate session
    public String logout() {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        return "main";
    }
    
}
