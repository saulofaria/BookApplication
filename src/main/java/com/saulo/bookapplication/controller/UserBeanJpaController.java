/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saulo.bookapplication.controller;

import com.saulo.bookapplication.DAO.DataConnect;
import com.saulo.bookapplication.controller.exceptions.NonexistentEntityException;
import com.saulo.bookapplication.controller.exceptions.RollbackFailureException;
import com.saulo.bookapplication.bean.UserBean;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Saulo
 */
@Named
@RequestScoped

public class UserBeanJpaController implements Serializable {


    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "PUforApp")
    private EntityManager em;

//    
//    public UserBeanJpaController(UserTransaction utx, EntityManagerFactory emf) {
//        this.utx = utx;
//        this.emf = emf;
//    }
//    private UserTransaction utx = null;
//    private EntityManagerFactory emf = null;
//
//    public EntityManager getEntityManager() {
//        return emf.createEntityManager();
//    }
    
    
    

    public static boolean validate(String uname, String password) {
        Connection con = null;
        PreparedStatement ps = null;

        try {
            con = DataConnect.getConnection();
            ps = con.prepareStatement("Select uname, password from Users where uname = ? and password = ?");
            ps.setString(1, uname);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                //result found, means valid inputs
                return true;
            }
        } catch (SQLException ex) {
            System.out.println("Login error -->" + ex.getMessage());
            return false;
        } finally {
            DataConnect.close(con);
        }
        return false;
    }
    
    
    
    
    
    public void create(UserBean userBean) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
//            em = getEntityManager();
            em.persist(userBean);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
//        finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public void edit(UserBean userBean) throws NonexistentEntityException, RollbackFailureException, Exception {
//        EntityManager em = null;
        try {
            utx.begin();
//            em = getEntityManager();
            userBean = em.merge(userBean);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = userBean.getUid();
                if (findUserBean(id) == null) {
                    throw new NonexistentEntityException("The userBean with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
//        finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
//        EntityManager em = null;
        try {
            utx.begin();
//            em = getEntityManager();
            UserBean userBean;
            try {
                userBean = em.getReference(UserBean.class, id);
                userBean.getUid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userBean with id " + id + " no longer exists.", enfe);
            }
            em.remove(userBean);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
//        finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    public List<UserBean> findUserBeanEntities() {
        return findUserBeanEntities(true, -1, -1);
    }

    public List<UserBean> findUserBeanEntities(int maxResults, int firstResult) {
        return findUserBeanEntities(false, maxResults, firstResult);
    }

    private List<UserBean> findUserBeanEntities(boolean all, int maxResults, int firstResult) {
//        EntityManager em = getEntityManager();
//        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UserBean.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
//        } 
//        finally {
//            em.close();
//        }
    }

    public UserBean findUserBean(Integer id) {
//        EntityManager em = getEntityManager();
//        try {
            return em.find(UserBean.class, id);
//        } finally {
//            em.close();
//        }
    }

    public int getUserBeanCount() {
//        EntityManager em = getEntityManager();
//        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UserBean> rt = cq.from(UserBean.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
//        } finally {
//            em.close();
//        }
    }

}
