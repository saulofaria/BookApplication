/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saulo.bookapplication.bean;

import com.saulo.bookapplication.entity.Inventory;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

/**
 *
 * @author sf
 */
@ManagedBean(name="themeService", eager = true)
@ApplicationScoped

public class ThemeService {

    private List<Inventory> themes;

    public List<Inventory> getThemes() {
        return themes;
    }
}
