/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saulo.bookapplication.bean;

import com.saulo.bookapplication.entity.Inventory;
import com.saulo.bookapplication.bean.ThemeService;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author sf
 */
@ManagedBean
public class AutoCompleteView {

    private List<Inventory> selectedThemes;

    @ManagedProperty("#{themeService}")
    private ThemeService service;

    public List<String> completeText(String query) {
        List<String> results = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            results.add(query + i);
        }

        return results;
    }

    public List<Inventory> completeTheme(String query) {
        List<Inventory> allThemes = service.getThemes();
        List<Inventory> filteredThemes = new ArrayList<Inventory>();

        for (int i = 0; i < allThemes.size(); i++) {
            Inventory skin = allThemes.get(i);
            if (skin.getTitle().toLowerCase().startsWith(query)) {
                filteredThemes.add(skin);
            }
        }

        return filteredThemes;
    }

    public void onItemSelect(SelectEvent event) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Item Selected", event.getObject().toString()));
    }

   

    public List<Inventory> getSelectedThemes() {
        return selectedThemes;
    }

    public void setSelectedThemes(List<Inventory> selectedThemes) {
        this.selectedThemes = selectedThemes;
    }

    public void setService(ThemeService service) {
        this.service = service;
    }

    public char getThemeGroup(Inventory inventory) {
        return inventory.getTitle().charAt(0);
    }

}
